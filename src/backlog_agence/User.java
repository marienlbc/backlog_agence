package backlog_agence;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.Id;

@Stateless
@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private String idUser;
	private String nomUser;
	private String prenomUser;
	private String pseudoUser;
	
	public User() {
		super();
	}
	
	public User(String idUser, String nomUser, String prenomUser, String pseudoUser) {
		super();
		this.idUser = idUser;
		this.nomUser = nomUser;
		this.prenomUser = prenomUser;
		this.pseudoUser = pseudoUser;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", nomUser=" + nomUser + ", prenomUser=" + prenomUser + ", pseudoUser="
				+ pseudoUser + "]";
	}

	public String getidUser() {
		return idUser;
	}
	public void setidUser(String idUser) {
		this.idUser = idUser;
	}
	public String getNomUser() {
		return nomUser;
	}
	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}
	public String getPrenomUser() {
		return prenomUser;
	}
	public void setPrenomUser(String prenomUser) {
		this.prenomUser = prenomUser;
	}
	public String getPseudoUser() {
		return pseudoUser;
	}
	public void setPseudoUser(String pseudoUser) {
		this.pseudoUser = pseudoUser;
	}
	
	
}
