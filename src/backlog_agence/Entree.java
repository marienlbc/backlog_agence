package backlog_agence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Stateless
@Entity
public class Entree implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private String idEntree;
	private String nomEntree;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationEntree;
	private int prioriteEntree;
	private int estimationEntree;
	@OneToMany @OrderBy("creationCommentaire ASC")
	private List<Commentaire> commentaires;
		
	public Entree() {
		super();
	}
	
	public Entree(String idEntree, String nomEntree, Date creationEntree, int prioriteEntree, int estimationEntree) {
		super();
		this.idEntree = idEntree;
		this.nomEntree = nomEntree;
		this.creationEntree = creationEntree;
		this.prioriteEntree = prioriteEntree;
		this.estimationEntree = estimationEntree;
		this.commentaires = commentaires;
	}

	
	@Override
	public String toString() {
		return "Entree [idEntree=" + idEntree + ", nomEntree=" + nomEntree + ", creationEntree=" + creationEntree
				+ ", prioriteEntree=" + prioriteEntree + ", estimationEntree=" + estimationEntree + ", commentaires="
				+ commentaires + "]";
	}

	public List<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public String getIdEntree() {
		return idEntree;
	}
	public void setIdEntree(String idEntree) {
		this.idEntree = idEntree;
	}
	public String getNomEntree() {
		return nomEntree;
	}
	public void setNomEntree(String nomEntree) {
		this.nomEntree = nomEntree;
	}
	public Date getCreationEntree() {
		return creationEntree;
	}
	public void setCreationEntree(Date creationEntree) {
		this.creationEntree = creationEntree;
	}
	public int getPrioriteEntree() {
		return prioriteEntree;
	}
	public void setPrioriteEntree(int prioriteEntree) {
		this.prioriteEntree = prioriteEntree;
	}
	public int getEstimationEntree() {
		return estimationEntree;
	}
	public void setEstimationEntree(int estimationEntree) {
		this.estimationEntree = estimationEntree;
	}
}
