package backlog_agence;

import java.util.Date;
import java.util.List;

public interface Operation {

	Agence createAgence(String idAgence, String nomAgence);
	
	Agence editAgence(String idAgence, String newNomAgence);
	
	Agence 	addEntree(Entree e);
	
	void selectAgence(String id);
	
	User createUser(String idUser, String nomUser, String prenomUser, String pseudoUser);

	User editUser(String idUser, String nomUser, String prenomUser, String pseudoUser);
	
	Entree createEntree(String idEntree, String nomEntree, Date creationEntree, int prioriteEntree, int estimationEntree);

	Entree editEntree(String idEntree, String nomEntree, Date creationEntree, int prioriteEntree, int estimationEntree);
	
	Commentaire createCommentaire(String idCommentaire, String contenuCommentaire, Date creationCommentaire, User auteur);
	
	Commentaire editCommentaire(String idCommentaire, String contenuCommentaire, Date creationCommentaire, User auteur);
	
	List<Agence> getListeAgence();
}
