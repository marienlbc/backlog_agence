package backlog_agence;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Stateless
@Entity
public class Commentaire implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private String idCommentaire;
	private String contenuCommentaire;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationCommentaire;
	@OneToOne
	private User auteur;

	
	public Commentaire(String idCommentaire, String contenuCommentaire, Date creationCommentaire, User auteur) {
		super();
		this.idCommentaire = idCommentaire;
		this.contenuCommentaire = contenuCommentaire;
		this.creationCommentaire = creationCommentaire;
		this.auteur = auteur;
	}

	public Commentaire() {
		super();
	}
	
	@Override
	public String toString() {
		return "Commentaire [idCommentaire=" + idCommentaire + ", contenuCommentaire=" + contenuCommentaire
				+ ", creationCommentaire=" + creationCommentaire + ", auteur=" + auteur + "]";
	}

		
	public User getAuteur() {
		return auteur;
	}

	public void setAuteur(User auteur) {
		this.auteur = auteur;
	}

	public String getIdCommentaire() {
		return idCommentaire;
	}
	public void setIdCommentaire(String idCommentaire) {
		this.idCommentaire = idCommentaire;
	}
	public String getContenuCommentaire() {
		return contenuCommentaire;
	}
	public void setContenuCommentaire(String contenuCommentaire) {
		this.contenuCommentaire = contenuCommentaire;
	}
	public Date getCreationCommentaire() {
		return creationCommentaire;
	}
	public void setCreationCommentaire(Date creationCommentaire) {
		this.creationCommentaire = creationCommentaire;
	}
	
	
	
	
}
