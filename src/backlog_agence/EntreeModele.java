package backlog_agence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Named
@SessionScoped
public class EntreeModele implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private Operation ejb;
	
	private String idEntree;
	private String nomEntree;
	@Temporal(TemporalType.TIMESTAMP)	
	private Date creationEntree;
	private int prioriteEntree;
	private int estimationEntree;
	private List<Commentaire> commentaires;
	
	
	
	public String getIdEntree() {
		return idEntree;
	}



	public void setIdEntree(String idEntree) {
		this.idEntree = idEntree;
	}



	public String getNomEntree() {
		return nomEntree;
	}



	public void setNomEntree(String nomEntree) {
		this.nomEntree = nomEntree;
	}



	public Date getCreationEntree() {
		return creationEntree;
	}



	public void setCreationEntree(Date creationEntree) {
		this.creationEntree = creationEntree;
	}



	public int getPrioriteEntree() {
		return prioriteEntree;
	}



	public void setPrioriteEntree(int prioriteEntree) {
		this.prioriteEntree = prioriteEntree;
	}



	public int getEstimationEntree() {
		return estimationEntree;
	}



	public void setEstimationEntree(int estimationEntree) {
		this.estimationEntree = estimationEntree;
	}



	public List<Commentaire> getCommentaires() {
		return commentaires;
	}



	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}



	public String creationEntree() {
		Entree e = ejb.createEntree(idEntree, nomEntree, creationEntree, prioriteEntree, estimationEntree);
		ejb.addEntree(e);
		return "index";
	}
}
