package backlog_agence;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class AgenceModele implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private Operation ejb;
	
	private String nom;
	private String id;
	private String selectedAgence;
	
	public String getSelectedAgence() {
		return selectedAgence;
	}
	public void setSelectedAgence(String selectedAgence) {
		this.selectedAgence = selectedAgence;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String creationAgence() {
		ejb.createAgence(id, nom);
		System.out.println("Creation Agence (modele) id : " + id + " nom " + nom);
		return "index";
	}
	
	public List<Agence> getListAgence(){
		return ejb.getListeAgence();
	}
	
	public String selectAgence() {
		ejb.selectAgence(selectedAgence);
		return "form_entree";
	}

}
