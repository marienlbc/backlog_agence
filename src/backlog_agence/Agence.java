package backlog_agence;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@NamedQuery(name="allAgences", query="select a from Agence a")

@Stateless
@Entity
public class Agence implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private String idAgence;
	private String nomAgence;
	@OneToMany @OrderBy("prioriteEntree DESC")
	private List<Entree> entrees;
	
	public Agence() {
		super();
	}
		
	public Agence(String idAgence, String nomAgence) {
		this.idAgence = idAgence;
		this.nomAgence = nomAgence;
	}	
	
	@Override
	public String toString() {
		return "Agence [idAgence=" + idAgence + ", nomAgence=" + nomAgence + "]";
	}

	public String getIdAgence() {
		return idAgence;
	}
	public void setIdAgence(String idAgence) {
		this.idAgence = idAgence;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}

	public List<Entree> getEntrees() {
		return entrees;
	}

	public void setEntrees(List<Entree> entrees) {
		this.entrees = entrees;
	}
	
	
}
