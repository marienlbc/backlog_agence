package backlog_agence;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class OperationBean implements Operation{

	@PersistenceContext
	private EntityManager em;
	
	private String idAgence;
	
	public OperationBean() {
		super();
	}
	
	@Override
	public Agence createAgence(String idAgence, String nomAgence) {
		Agence a = new Agence(idAgence, nomAgence);
		em.persist(a);
		System.out.println("Creation Agence (operationbean) id : " + idAgence + " nom " + nomAgence);
		return a;
	}

	@Override
	public Agence editAgence(String idAgence, String newNomAgence) {
		Agence a = em.find(Agence.class, idAgence);
		a.setNomAgence(newNomAgence);
		return a;
	}

	@Override
	public User createUser(String idUser, String nomUser, String prenomUser, String pseudoUser) {
		User u = new User(idUser, nomUser, prenomUser, pseudoUser);
		em.persist(u);
		return u;
	}

	@Override
	public User editUser(String idUser, String nomUser, String prenomUser, String pseudoUser) {
		User u = em.find(User.class, idUser);
		u.setNomUser(nomUser);
		u.setPrenomUser(prenomUser);
		u.setPseudoUser(pseudoUser);
		return u;
	}

	@Override
	public Entree createEntree(String idEntree, String nomEntree, Date creationEntree, int prioriteEntree,
			int estimationEntree) {
		Entree e = new Entree(idEntree, nomEntree, creationEntree, prioriteEntree, estimationEntree);
		em.persist(e);
		return e;
	}

	@Override
	public Entree editEntree(String idEntree, String nomEntree, Date creationEntree, int prioriteEntree,
			int estimationEntree) {
		Entree e = em.find(Entree.class, idEntree);
		e.setNomEntree(nomEntree);
		e.setPrioriteEntree(prioriteEntree);
		e.setEstimationEntree(estimationEntree);
		return e;
	}

	@Override
	public Commentaire createCommentaire(String idCommentaire, String contenuCommentaire, Date creationCommentaire,
			User auteur) {
		Commentaire c = new Commentaire(idCommentaire, contenuCommentaire, creationCommentaire, auteur);
		em.persist(c);
		return c;
	}

	@Override
	public Commentaire editCommentaire(String idCommentaire, String contenuCommentaire, Date creationCommentaire,
			User auteur) {
		Commentaire c = em.find(Commentaire.class, idCommentaire);
		c.setContenuCommentaire(contenuCommentaire);
		return c;
	}
	
    @Override
	public List<Agence> getListeAgence(){
    	Query q = em.createNamedQuery("allAgences");
    	return q.getResultList();
    }

	@Override
	public Agence addEntree(Entree e) {
		System.out.println("id agence : "+idAgence);
		Agence a = em.find(Agence.class, idAgence);
		List<Entree> tmpEntrees = a.getEntrees();
		tmpEntrees.add(e);
		a.setEntrees(tmpEntrees);
		return a;
	}

	@Override
	public void selectAgence(String id) {
		idAgence = id;
	}	
	
}
